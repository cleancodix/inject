
## [1.0.0] 12-02-2024

First release

## [1.0.1] 13-02-2024

Added .gitignore to project and cleaned up some files

## [1.0.2] 06-03-2024

Now Inject will call .dispose() methods of any injected dependencies
Before Inject would have called only .dispose() methods on dependencies with the Disposable mixin
And so, Disposable mixin has been removed

## [1.0.3] 25-03-2024

Fixed InjectAll builder's given context when built