/// A simple dependency injection widget based on standard Flutter's InheritedWidget
library flutter_inject;

export 'src/inject.dart';
