part of 'inject.dart';

/// The simple [InheritedWidget] behind this package
class _Injected<T> extends InheritedWidget {
  /// Retrieve any previously injected dependency with the given [Type] T
  /// Note that null will be returned if no dependency have been found
  static T? get<T>(BuildContext context) {
    final injected = context.dependOnInheritedWidgetOfExactType<_Injected<T>>();
    return injected?.instance;
  }

  /// The injected instance of our dependency
  final T instance;

  /// The constructor of our [InheritedWidget] widget
  const _Injected(this.instance, {super.key, required super.child});

  /// We do not need to rebuild widgets that depends on this one when this one is rebuilt
  @override
  bool updateShouldNotify(_Injected<T> oldWidget) => false;
}

/// The [InheritedWidget] that [Inject] will check before injecting a new instance
class _Override<T> extends _Injected<T> {
  /// Retrieve any previously injected dependency with the given [Type] T
  /// Note that null will be returned if no dependency have been found
  static T? get<T>(BuildContext context) {
    final override = context.dependOnInheritedWidgetOfExactType<_Override<T>>();
    return override?.instance;
  }

  /// The constructor of our [InheritedWidget] widget
  const _Override(super.instance, {required super.child});
}
