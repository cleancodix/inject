part of 'inject.dart';

///
/// The [Dependency] class allows you to retrieve any dependency previously injected:
/// ```dart
/// final repository = Dependency.get<MyRepository>(context);
/// ```
///
/// The [Dependency] class allows you to inject many dependencies at once within the [InjectAll] widget:
/// ```dart
/// class MyApp extends StatelessWidget {
///   const MyApp({super.key});
///
///   @override
///   Widget build(BuildContext context) {
///     // Inject multiple dependencies
///     return InjectAll(
///       dependencies: [
///         Dependency<MyApi>((context) => MyApiImpl()),
///         Dependency<MyRepository>((context) => MyRepositoryImpl(
///           // You can depend on any dependency previsouly injected
///           api: Dependency.get<MyApi>(context),
///         )),
///       ],
///       builder: (context) {
///         // Then retrieve your dependencies anywhere in the widget tree
///         final repository = Dependency.get<MyRepository>(context);
///         return MyView();
///       },
///     );
///   }
/// }
/// ```
///
class Dependency<T> {
  /// Retrieve any previously injected dependency with the given [Type] T
  /// Note that an exception will be thrown if no dependency have been found
  static T get<T>(BuildContext context) {
    final dependency = maybeGet<T>(context);
    assert(dependency != null, 'No $T dependency found in context');
    return dependency!;
  }

  /// Retrieve any previously injected dependency with the given [Type] T
  /// Note that null will be returned if no dependency have been found
  static T? maybeGet<T>(BuildContext context) {
    return _Override.get<T>(context) ?? _Injected.get<T>(context);
  }

  /// A boolean that indicates if this dependency will override children's ones
  final bool override;

  /// A boolean that indicates if a previously injected dependency with the same [Type] T can be reused
  final bool useExistingInstance;

  /// The method which will return the instance of our dependency
  late final T Function(BuildContext) factory;

  /// The constructor of our [Dependency] class
  Dependency(
    this.factory, {
    this.override = false,
    this.useExistingInstance = false,
  });

  /// The method to wrap [builder] within an [Inject] widget in order to provide him our dependency
  Inject _inject(
    Widget Function(BuildContext) builder, {
    required bool? override,
    required bool? useExistingInstance,
  }) =>
      Inject<T>(
        factory: factory,
        builder: builder,
        override: override ?? this.override,
        useExistingInstance: useExistingInstance ?? this.useExistingInstance,
      );
}
