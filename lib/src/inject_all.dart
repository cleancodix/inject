part of 'inject.dart';

///
/// The widget that allows you to inject many dependencies at once glad to the [Dependency] class:
/// ```dart
/// class MyApp extends StatelessWidget {
///   const MyApp({super.key});
///
///   @override
///   Widget build(BuildContext context) {
///     // Inject multiple dependencies
///     return InjectAll(
///       dependencies: [
///         Dependency<MyApi>((context) => MyApiImpl()),
///         Dependency<MyRepository>((context) => MyRepositoryImpl(
///           // You can depend on any dependency previsouly injected
///           api: Dependency.get<MyApi>(context),
///         )),
///       ],
///       builder: (context) {
///         // Then retrieve your dependencies anywhere in the widget tree
///         final repository = Dependency.get<MyRepository>(context);
///         return MyView();
///       },
///     );
///   }
/// }
/// ```
///
class InjectAll extends StatefulWidget {
  /// A boolean that indicates if injected dependencies will override children's ones
  final bool _override;

  /// A boolean that indicates if a previously injected dependencies with the same [Type] T can be reused
  final bool useExistingInstance;

  /// A builder method that returns your child widget
  final Widget Function(BuildContext) builder;

  /// The [Dependency] array that holds factories to return instances of our dependencies
  final List<Dependency> dependencies;

  /// The constructor of our [InjectAll] widget
  const InjectAll({
    super.key,
    required this.builder,
    required this.dependencies,
    this.useExistingInstance = false,
    bool override = false,
  }) : _override = override;

  /// The [State] factory of this [StatefulWidget]
  @override
  State<InjectAll> createState() => _InjectAllState();
}

/// The [State] class of our [StatefulWidget] widget
class _InjectAllState extends State<InjectAll> {
  /// The "Inception" builder which will wrap [Inject] widget of each [Dependency] into each other
  late final Widget Function(BuildContext) nestedInjectorsBuilder;

  /// The initState method that init the [nestedInjectorsBuilder] property
  @override
  void initState() {
    super.initState();
    nestedInjectorsBuilder = widget.dependencies.reversed.fold(
      (context) => widget.builder(context),
      (a, b) => (context) {
        return b._inject(a, override: widget._override, useExistingInstance: widget.useExistingInstance);
      },
    );
  }

  /// The build method which will wrap your [builder] within the [nestedInjectorsBuilder] one
  @override
  Widget build(BuildContext context) => nestedInjectorsBuilder(context);
}
