import 'package:flutter/material.dart';

part 'dependency.dart';
part 'inherited.dart';
part 'inject_all.dart';

///
/// The widget that allows you to inject any dependency:
/// ```dart
/// class MyView extends StatelessWidget {
///   const MyView({super.key});
///
///   @override
///   Widget build(BuildContext context) {
///     // Inject one dependency
///     return Inject<MyLogic>(
///       factory: (context) => MyLogic(
///         // You can depend on any dependency previously injected
///         repository: Dependency.get<MyRepository>(context),
///       ),
///       builder: (context) {
///         // Then retrieve you dependency anywhere in the widget tree
///         final logic = Dependency.get<MyLogic>(context);
///         return Scaffold(body: Center(child: Text(child: logic.getTitle())));
///       },
///     );
///   }
/// }
/// ```
///
class Inject<T> extends StatefulWidget {
  /// A boolean that indicates if injected dependency will override children's ones
  final bool _override;

  /// A boolean that indicates if a previously injected dependency with the same [Type] T can be reused
  final bool useExistingInstance;

  /// A builder method that returns your child widget
  final Widget Function(BuildContext) builder;

  /// The method which will return the instance of our dependency
  final T Function(BuildContext) factory;

  /// The constructor of our [Inject] widget
  const Inject({
    super.key,
    required this.builder,
    required this.factory,
    this.useExistingInstance = false,
    bool override = false,
  }) : _override = override;

  /// The [State] factory of this [StatefulWidget]
  @override
  State<Inject<T>> createState() => _InjectState<T>();
}

/// The [State] class of our [StatefulWidget] widget
class _InjectState<T> extends State<Inject<T>> {
  /// The instance of our dependency to be injected
  T? instance;

  /// A boolean that indicates if our dependency instance should be disposed
  /// If it's true then it will be disposed when this [StatefulWidget] will be removed from the widget tree
  bool shouldDisposeInstance = true;

  /// The dispose method called when this [StatefulWidget] will be removed from the widget tree
  @override
  void dispose() {
    try {
      if (shouldDisposeInstance) (instance as dynamic).dispose();
    } catch (e) {
      // If [instance] is an object which doesn't have a dispose method
      // We'll get an exception so here we're just ignoring this exception
    } finally {
      super.dispose();
    }
  }

  /// The build method which will wrap your [builder] within our [InheritedWidget]
  @override
  Widget build(BuildContext context) {
    final override = _Override.get<T>(context) ?? (widget.useExistingInstance ? _Injected.get<T>(context) : null);
    shouldDisposeInstance = override == null;
    return widget._override
        ? _Override(
            instance ??= override ?? widget.factory(context),
            child: Builder(builder: widget.builder),
          )
        : _Injected(
            instance ??= override ?? widget.factory(context),
            child: Builder(builder: widget.builder),
          );
  }
}
