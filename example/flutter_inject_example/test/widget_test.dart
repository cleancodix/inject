import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inject/flutter_inject.dart';
import 'package:flutter_inject_example/main.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class _CounterLogicMock extends Mock implements CounterLogic {}

void main() {
  group('Original Flutter counter test', () {
    testWidgets('Counter increments', (WidgetTester tester) async {
      // Build our app and trigger a frame
      await tester.pumpWidget(const MaterialApp(home: CounterView()));

      // Verify that our counter starts at 0
      expect(find.text('Counter: 0'), findsOneWidget);
      expect(find.text('Counter: 1'), findsNothing);

      // Tap the '+' icon and trigger a frame
      await tester.tap(find.byIcon(Icons.add));
      await tester.pump();

      // Verify that our counter has incremented
      expect(find.text('Counter: 0'), findsNothing);
      expect(find.text('Counter: 1'), findsOneWidget);
    });
  });

  group("Override widget's dependency with a mocked one", () {
    late _CounterLogicMock mockedLogic;

    setUp(() {
      mockedLogic = _CounterLogicMock();
    });

    testWidgets('Counter increments', (WidgetTester tester) async {
      // Stub behaviour of logic's getters
      final controller = StreamController<int>();
      when(() => mockedLogic.state).thenAnswer((_) => 666);
      when(() => mockedLogic.controller).thenAnswer((_) => controller);

      // Build our app and trigger a frame
      await tester.pumpWidget(
        MaterialApp(
          // Inject a mocked version of our logic
          home: Inject<CounterLogic>(
            // Override children's CounterLogic injection
            // They will reuse this mocked logic through context instead of instantiating a new one
            override: true,
            factory: (context) => mockedLogic,
            builder: (context) => const CounterView(),
          ),
        ),
      );

      // Verify that our widget has been rendered correctly (counter starts at 666)
      expect(find.text('Counter: 666'), findsOneWidget);
      expect(find.text('Counter: 999'), findsNothing);

      // Stream a new value and trigger a frame
      controller.add(999);
      await tester.pump();

      // Verify our widget's behavior (logic's increment method should not have been called yet)
      verifyNever(() => mockedLogic.increment());

      // Verify that our widget has been rendered correctly (counter match new value)
      expect(find.text('Counter: 666'), findsNothing);
      expect(find.text('Counter: 999'), findsOneWidget);

      // Tap the '+' icon
      await tester.tap(find.byIcon(Icons.add));

      // Verify our widget's behavior (logic's increment method should have been called once)
      verify(() => mockedLogic.increment()).called(1);
    });
  });
}
