import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_inject/flutter_inject.dart';

void main() => runApp(const MaterialApp(home: CounterView()));

class CounterView extends StatelessWidget {
  const CounterView({super.key});

  @override
  Widget build(BuildContext context) {
    return Inject<CounterLogic>(
      factory: (context) => CounterLogic(),
      builder: (context) {
        final logic = Dependency.get<CounterLogic>(context);
        return Scaffold(
          appBar: AppBar(title: const Text('Counter demo')),
          body: Center(
            child: StreamBuilder(
              initialData: logic.state,
              stream: logic.controller.stream,
              builder: (context, snapshot) => Text('Counter: ${snapshot.data}'),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: logic.increment,
            child: const Icon(Icons.add),
          ),
        );
      },
    );
  }
}

class CounterLogic {
  int state = 0;
  final controller = StreamController<int>();

  void increment() => controller.add(++state);

  void dispose() => controller.close();
}
